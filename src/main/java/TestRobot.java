/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0, 0, 2, 4, 100);
        robot.walk('S');
        System.out.println(robot);
        robot.walk('N', 3);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk('W', 2);
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk(4);
        System.out.println(robot);
    }
}
